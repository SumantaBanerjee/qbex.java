package com.qbex.requests;

import com.google.gson.annotations.Expose;
import com.qbex.pojo.BaseObject;

public class LoginRequest extends BaseObject{
	@Expose
	private String emailId;
	@Expose
	private String password;
	public LoginRequest(String emailId, String password) {
		super();
		this.emailId = emailId;
		this.password = password;
	}
	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param emailId the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "LoginRequest [emailId=" + emailId + ", password=" + password + "]";
	}
	
}
