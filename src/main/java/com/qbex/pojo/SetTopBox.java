package com.qbex.pojo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.google.gson.annotations.Expose;

public class SetTopBox extends BaseObject implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name="ADDRESS_ID")
	@Expose
	private int id;
	
	@Column(name="STB_NO")
	String setTopBoxNumber;
	
	@Column(name="VC_NO")
	String vcNo;
	
}
