package com.qbex.pojo;

import com.google.gson.annotations.Expose;

public class ResponseObject extends BaseObject {
	@Expose
	public Object response;
	@Expose
	public int code;
	@Expose
	public String message;
	/**
	 * @return the response
	 */
	public Object getResponse() {
		return response;
	}
	/**
	 * @return the code
	 */
	public int getCode() {
		return code;
	}
	public ResponseObject(Object response, int code, String message) {
		super();
		this.response = response;
		this.code = code;
		this.message = message;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param response the response to set
	 */
	public void setResponse(Object response) {
		this.response = response;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(int code) {
		this.code = code;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
}
