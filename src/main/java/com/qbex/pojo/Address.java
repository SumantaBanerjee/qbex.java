package com.qbex.pojo;

import java.io.Serializable;

import javax.persistence.*;

import com.google.gson.annotations.Expose;
import com.qbex.util.Helper;

@Entity
@Table(name="address")
public class Address extends BaseObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static String ADDRESS_ID = "ADDRESS_ID";
	public static String ADDR_LINE_1 = "ADDR_LINE_1";
	public static String ADDR_LINE_2 = "ADDR_LINE_2";
	public static String CITY_NAME = "CITY_NAME";
	public static String DISTRICT = "DISTRICT";
	public static String STATE = "STATE";
	public static String PINCODE = "PINCODE";
	public static String COUNTRY = "COUNTRY";
	public static String CUSTOMER_ID = "CUSTOMER_ID";
	
	private static String INSERT_QUERY = "INSERT INTO address " + "(" + ADDRESS_ID + ", " + ADDR_LINE_1 + ", "
			+ ADDR_LINE_2 + ", " + CITY_NAME + ", " + DISTRICT + ", " + STATE + ", " + PINCODE + ", "+CUSTOMER_ID+")"
			+ " VALUES " + "(" + ":" + ADDRESS_ID + ", " + ":" + ADDR_LINE_1 + ", " + ":" + ADDR_LINE_2 + ", " + ":"
			+ CITY_NAME + ", " + ":" + DISTRICT + ", " + ":" + STATE + ", " + ":" + PINCODE + ", " + ":"+CUSTOMER_ID
			+ ")";
	
	@Id
	@Column(name="ADDRESS_ID")
	@Expose
	private String id;
	@Column(name="ADDR_LINE_1")
	@Expose
	private String addressLineOne;
	@Column(name="ADDR_LINE_2")
	@Expose
	private String addressLineTwo;
	@Column(name="CITY_NAME")
	@Expose
	private String cityname;
	@Column(name="DISTRICT")
	@Expose
	private String district;
	@Column(name="STATE")
	@Expose
	private String state;
	@Column(name="PINCODE")
	@Expose
	private String pincode;
	@Column(name="COUNTRY")
	@Expose
	private String country;
	@Column(name="CUSTOMER_ID")
	private String customerId;
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @return the addrLine1
	 */
	public String getAddressLineOne() {
		return addressLineOne;
	}
	/**
	 * @return the addrLine2
	 */
	public String getAddressLineTwo() {
		return addressLineTwo;
	}
	/**
	 * @return the cityname
	 */
	public String getCityname() {
		return cityname;
	}
	/**
	 * @return the district
	 */
	public String getDistrict() {
		return district;
	}
	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	/**
	 * @return the pincode
	 */
	public String getPincode() {
		return pincode;
	}
	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @param addrLine1 the addrLine1 to set
	 */
	public void setAddressLineOne(String addrLine1) {
		this.addressLineOne = addrLine1;
	}
	/**
	 * @param addrLine2 the addrLine2 to set
	 */
	public void setAddressLineTwo(String addrLine2) {
		this.addressLineTwo = addrLine2;
	}
	/**
	 * @param cityname the cityname to set
	 */
	public void setCityname(String cityname) {
		this.cityname = cityname;
	}
	/**
	 * @param district the district to set
	 */
	public void setDistrict(String district) {
		this.district = district;
	}
	/**
	 * @param state the state to set 
	 */
	public void setState(String state) {
		this.state = state;
	}
	/**
	 * @param pincode the pincode to set
	 */
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}
	/**
	 * @return the customerId
	 */
	public String getCustomerId() {
		return customerId;
	}
	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	
	public String insertAddress(EntityManager entityManager) throws PersistenceException {
		String addressId = Helper.getUniqueId();
		Query query = entityManager.createNativeQuery(INSERT_QUERY);
		query.setParameter(ADDRESS_ID, addressId);
		query.setParameter(CUSTOMER_ID, this.getCustomerId());
		query.setParameter(ADDR_LINE_1, this.getAddressLineOne());
		query.setParameter(ADDR_LINE_2, this.getAddressLineTwo());
		query.setParameter(CITY_NAME, this.getCityname());
		query.setParameter(DISTRICT, this.getDistrict());
		query.setParameter(STATE, this.getState());
		query.setParameter(PINCODE, this.getPincode());
		query.setParameter(COUNTRY, this.getCountry());
		System.out.println(query.toString());
		query.executeUpdate();
		
		return addressId;
	}
}
