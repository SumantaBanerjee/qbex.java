package com.qbex.pojo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class BaseObject {
	private Gson gson;
	public BaseObject(){
		gson=new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
	}
	public String toJson(){
		try{
			return gson.toJson(this);
		}catch(NullPointerException ex){
			return "{}";
		}
	}
}
