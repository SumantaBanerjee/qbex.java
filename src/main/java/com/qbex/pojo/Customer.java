package com.qbex.pojo;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.Table;
import javax.persistence.TypedQuery;

import org.hibernate.annotations.Cascade;
import org.hibernate.engine.transaction.jta.platform.internal.SynchronizationRegistryBasedSynchronizationStrategy;

import com.google.gson.annotations.Expose;
import com.qbex.util.Helper;

@Entity
@Table(name = "customer")
public class Customer extends BaseObject implements Serializable {
	/**
	 * 
	 */

	public static String CUSTOMER_ID = "CUSTOMER_ID";

	public static String FIRST_NAME = "FIRST_NAME";
	public static String MIDDLE_NAME = "MIDDLE_NAME";
	public static String LAST_NAME = "LAST_NAME";
	public static String JOINED_ON = "JOINED_ON";
	public static String STATUS = "STATUS";
	public static String CREATE_DATE = "CREATE_DATE";
	public static String UPDATE_DATE = "UPDATE_DATE";
	public static String EMAILID = "EMAILID";
	public static String PASSWORD = "PASSWORD";
	public static String DISCOUNT = "DISCOUNT";
	public static String SMSID = "SMSID";
	public static String PHONE = "PHONE";

	private static String INSERT_QUERY = "INSERT INTO customer " + "(" + CUSTOMER_ID + ", " + FIRST_NAME + ", "
			+ MIDDLE_NAME + ", " + LAST_NAME + ", " + JOINED_ON + ", " + STATUS + ", " + CREATE_DATE + ", "
			+ UPDATE_DATE + ", " + EMAILID + ", " + PASSWORD + ", " + DISCOUNT + ", " + SMSID + ", " + PHONE + ")"
			+ " VALUES " + "(" + ":" + CUSTOMER_ID + ", " + ":" + FIRST_NAME + ", " + ":" + MIDDLE_NAME + ", " + ":"
			+ LAST_NAME + ", " + ":" + JOINED_ON + ", " + ":" + STATUS + ", " + ":" + CREATE_DATE + ", " + ":"
			+ UPDATE_DATE + ", " + ":" + EMAILID + ", " + ":" + PASSWORD + ", " + ":" + DISCOUNT + ", " + ":" + SMSID
			+ ", " + ":" + PHONE + "" + ")";
	private static String UPDATE_QUERY = "UPDATE customer SET ";
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "CUSTOMER_ID")
	@Expose
	private String id;
	@Column(name = "FIRST_NAME")
	@Expose
	private String firstName;
	@Column(name = "MIDDLE_NAME")
	@Expose
	private String middleName;
	@Column(name = "LAST_NAME")
	@Expose
	private String lastName;
	@Column(name = "JOINED_ON")
	@Expose
	private Date joinedOn;
	@Column(name = "STATUS")
	@Expose
	private String status;
	@Column(name = "CREATE_DATE")
	@Expose
	private Timestamp createdOn;
	@Column(name = "UPDATE_DATE")
	@Expose
	private Timestamp updatedOn;
	@Column(name = "EMAILID")
	@Expose
	private String emailId;
	@Column(name = "PASSWORD")
	@Expose
	private String password;
	@Column(name = "DISCOUNT")
	@Expose
	private Double discount;
	@Column(name = "SMSID")
	@Expose
	private String smsId;
	@Column(name = "PHONE")
	@Expose
	private String phone;

	public Customer() {
		super();
		this.id = null;
		this.createdOn = null;
		this.discount = null;
		this.emailId = null;
		this.firstName = null;
		this.joinedOn = null;
		this.lastName = null;
		this.middleName = null;
		this.password = null;
		this.phone = null;
		this.smsId = null;
		this.status = null;
		this.updatedOn = null;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @return the joinedOn
	 */
	public Date getJoinedOn() {
		return joinedOn;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @return the createdOn
	 */
	public Timestamp getCreatedOn() {
		return createdOn;
	}

	/**
	 * @return the updatedOn
	 */
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @param middleName
	 *            the middleName to set
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @param joinedOn
	 *            the joinedOn to set
	 */
	public void setJoinedOn(Date joinedOn) {
		this.joinedOn = joinedOn;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @param createdOn
	 *            the createdOn to set
	 */
	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @param updatedOn
	 *            the updatedOn to set
	 */
	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param emailId
	 *            the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the discount
	 */
	public Double getDiscount() {
		return discount;
	}

	/**
	 * @return the smsId
	 */
	public String getSmsId() {
		return smsId;
	}

	/**
	 * @param discount
	 *            the discount to set
	 */
	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	/**
	 * @param smsId
	 *            the smsId to set
	 */
	public void setSmsId(String smsId) {
		this.smsId = smsId;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone
	 *            the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String insertCustomer(EntityManager entityManager) throws PersistenceException {
		String customerId = Helper.getUniqueId();
		Query query = entityManager.createNativeQuery(INSERT_QUERY);
		query.setParameter(CUSTOMER_ID, customerId);
		query.setParameter(FIRST_NAME, this.getFirstName());
		query.setParameter(MIDDLE_NAME, this.getMiddleName());
		query.setParameter(LAST_NAME, this.getLastName());
		query.setParameter(JOINED_ON, this.getJoinedOn());
		query.setParameter(STATUS, this.getStatus());
		query.setParameter(CREATE_DATE, this.getCreatedOn());
		query.setParameter(UPDATE_DATE, this.getUpdatedOn());
		query.setParameter(EMAILID, this.getEmailId());
		query.setParameter(PASSWORD, this.getPassword());
		query.setParameter(DISCOUNT, this.getDiscount());
		query.setParameter(SMSID, this.getSmsId());
		query.setParameter(PHONE, this.getPhone());
		System.out.println(query.toString());
		query.executeUpdate();

		return customerId;
	}

	public void update(EntityManager entityManager, String customer_id) throws PersistenceException {
		// Convert POJO to Map
		Map<String, Object> object = new HashMap<String, Object>();
		if (this.getFirstName() != null) {
			object.put(FIRST_NAME, this.getFirstName());
		}
		if (this.getMiddleName() != null) {
			object.put(MIDDLE_NAME, this.getMiddleName());
		}
		if (this.getLastName() != null) {
			object.put(LAST_NAME, this.getLastName());
		}
		if (this.getJoinedOn() != null) {
			object.put(JOINED_ON, this.getJoinedOn());
		}
		if (this.getStatus() != null) {
			object.put(STATUS, this.getStatus());
		}
		if (this.getCreatedOn() != null) {
			object.put(CREATE_DATE, this.getCreatedOn());
		}
		if (this.getUpdatedOn() != null) {
			object.put(UPDATE_DATE, Helper.getCurresntTimestamp());
		}
		if (this.getEmailId() != null) {
			object.put(EMAILID, this.getEmailId());
		}
		if (this.getPassword() != null) {
			object.put(PASSWORD, this.getPassword());
		}
		if (this.getDiscount() != null) {
			object.put(DISCOUNT, this.getDiscount());
		}
		if (this.getSmsId() != null) {
			object.put(SMSID, this.getSmsId());
		}
		if (this.getPhone() != null) {
			object.put(PHONE, this.getPhone());
		}

		Set<String> keySet = object.keySet();

		// Prepare SQL

		String sql = Helper.generateSqlStatement("customer", object);
		sql += " WHERE " + CUSTOMER_ID + "=:" + CUSTOMER_ID;

		System.out.println(sql);

		Query query = entityManager.createNativeQuery(sql);

		for (String key : keySet) {
			query.setParameter(key, object.get(key));
		}
		query.setParameter(CUSTOMER_ID, customer_id);

		query.executeUpdate();
	}

	public Customer getParams(EntityManager entityManager, List<String> params, HashMap<String, Object> where) throws PersistenceException {
		Customer customer = new Customer();
		String sql = Helper.buildSelectQuery(params, where, "customer");
			System.out.println(sql);

			Query query = entityManager.createNativeQuery(sql, Customer.class);
			if(where!=null && where.size()>0){
				for(String key : where.keySet()){
					query.setParameter(key, where.get(key));
				}
			}
			customer = (Customer) query.getSingleResult();
		return customer;
	}
	

}
