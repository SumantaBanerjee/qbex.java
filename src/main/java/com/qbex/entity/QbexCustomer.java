package com.qbex.entity;

import com.google.gson.annotations.Expose;
import com.qbex.pojo.Address;
import com.qbex.pojo.Customer;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;

public class QbexCustomer extends Customer{
	@Expose
	private Set<Address> addresses;

	public QbexCustomer(Customer customer){
		super();
		this.setFirstName(customer.getFirstName());
		this.setMiddleName(customer.getMiddleName());
		this.setLastName(customer.getLastName());
		this.setJoinedOn(customer.getJoinedOn());
		this.setStatus(customer.getStatus());
		this.setCreatedOn(customer.getCreatedOn());
		this.setEmailId(customer.getEmailId());
		//this.setPassword(customer.setPassword(password));
		this.setDiscount(customer.getDiscount());
		this.setSmsId(customer.getSmsId());
		this.setPhone(customer.getPhone());
	}
	
	/**
	 * @return the addresses
	 */
	public Set<Address> getAddresses() {
		return addresses;
	}

	/**
	 * @param addresses the addresses to set
	 */
	public void setAddresses(Set<Address> addresses) {
		this.addresses = addresses;
	}

	/* (non-Javadoc)
	 * @see com.qbex.pojo.Customer#update(javax.persistence.EntityManager, java.lang.String)
	 */
	@Override
	public void update(EntityManager entityManager, String customer_id) throws PersistenceException {
		// TODO Auto-generated method stub
		super.update(entityManager, customer_id);
	}
}
