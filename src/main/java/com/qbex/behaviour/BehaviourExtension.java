package com.qbex.behaviour;

public interface BehaviourExtension<T>{
	public abstract T preIxn(T obj);
	public void postIxn(T obj);
}
