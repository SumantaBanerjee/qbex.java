package com.qbex.behaviour;

import org.apache.commons.codec.digest.DigestUtils;

import com.qbex.pojo.Customer;

public class TxnCustomerExtn implements BehaviourExtension<Customer> {

	public Customer preIxn(Customer obj) {
		// TODO Auto-generated method stub
		System.out.println("TxnCustomerExtn : PreIxn");
		String password=obj.getPassword();
		if(password!=null){
			password = DigestUtils.sha1Hex(password);
		}
		obj.setPassword(password);
		return obj;
	}

	public void postIxn(Customer obj) {
		// TODO Auto-generated method stub
		System.out.println("TxnCustomerExtn : PostIxn");
		
	}

}
