package com.qbex.controllers;

import javax.naming.NamingException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("/")
public class API {
	@GET
	@Path("/")
	@Produces("application/json")
	public Response getEmployeeById() throws NamingException {
		return Response.status(200).entity("Qbex.Cable").build();
	}
}
