package com.qbex.controllers;

import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.Produces;

@Produces("application/json")
public enum ResourceBase {
	INSTANCE;
	private EntityManagerFactory emf;

	private ResourceBase() {
		emf = Persistence.createEntityManagerFactory("qbexEntityManager");
	}

	public EntityManager getEntityManager() throws NamingException {
		return emf.createEntityManager();
	}
	
	public void close(){
		emf.close();
	}
}
