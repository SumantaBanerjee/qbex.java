package com.qbex.controllers;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.qbex.behaviour.BehaviourExtension;
import com.qbex.entity.QbexCustomer;
import com.qbex.pojo.Address;
import com.qbex.pojo.Customer;
import com.qbex.pojo.ResponseObject;
import com.qbex.requests.LoginRequest;
import com.qbex.transactions.TxnBaseObject;
import com.qbex.transactions.TxnCustomer;
import com.sun.jersey.core.impl.provider.entity.FormProvider;
import com.unmarshaller.Unmarshaller;
import com.unmarshaller.UnmarshallingException;

/**
 * This class handles the Customer entity.
 * 
 * @author Sumanta.Banerjee 5/14/2017
 */

@Path("/customers")
public class Customers {
	@Context
	private ServletContext context;

	private EntityManager entityManager;

	/**
	 * This method return one customer with the given id
	 * 
	 * @param id
	 *            if of the requested customer
	 * @return returns the customer JSON Object
	 * @throws NamingException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws ClassNotFoundException
	 */
	@GET
	@Path("/getCustomer/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCustomerById(@PathParam("id") String id)
			throws NamingException, ClassNotFoundException, InstantiationException, IllegalAccessException {
		entityManager = ResourceBase.INSTANCE.getEntityManager();
		TxnCustomer txnCustomer = new TxnCustomer(entityManager);
		QbexCustomer customer = txnCustomer.getCustomerById(id);
		ResponseObject responseObject = new ResponseObject(customer, 200, "success");
		if (customer == null) {
			responseObject = new ResponseObject(null, 404, "not found");
		}
		return Response.status(200).entity(responseObject.toJson()).build();
	}

	@POST
	@Path("/insertCustomer")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response insertCustomer(String request)
			throws NamingException, ClassNotFoundException, InstantiationException, IllegalAccessException {
		ResponseObject responseobj = null;
		entityManager = ResourceBase.INSTANCE.getEntityManager();
		System.out.println(entityManager);
		QbexCustomer customer = null;
		TxnCustomer customerTransaction = new TxnCustomer(entityManager);
		long millis1 = System.currentTimeMillis();
		try {
			customer = (QbexCustomer) Unmarshaller.unmarshal(request, QbexCustomer.class);
		} catch (UnmarshallingException e) {
			responseobj = new ResponseObject(customer, 200, "validation failed");
			return Response.status(200).entity(responseobj.toJson()).build();
		}
		try {
			customerTransaction.insertCustomer(customer);
			responseobj = new ResponseObject(customer, 200, "customer created");
		} catch (PersistenceException e) {
			responseobj = new ResponseObject(null, 400, e.getMessage());
		}
		long millis2 = System.currentTimeMillis();
		System.out.println("Time taken in millis: " + (millis2 - millis1) + " ms");
		return Response.status(200).entity(responseobj.toJson()).build();
	}

	@POST
	@Path("/updateCustomer/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCustomer(@PathParam("id") String id, String request)
			throws NamingException, ClassNotFoundException, InstantiationException, IllegalAccessException, UnmarshallingException {
		entityManager = ResourceBase.INSTANCE.getEntityManager();
		ResponseObject responseobj;
		TxnCustomer txnCustomer = new TxnCustomer(entityManager);
		QbexCustomer customer = (QbexCustomer) Unmarshaller.unmarshal(request, QbexCustomer.class);
		customer.setId(id);
		try{
			txnCustomer.updateCustomer(customer);
		}catch (PersistenceException e) {
			responseobj = new ResponseObject(null, javax.servlet.http.HttpServletResponse.SC_EXPECTATION_FAILED, e.getMessage());
			return Response.status(2400).entity(customer.toJson()).build();
		}
		responseobj = new ResponseObject(customer, javax.servlet.http.HttpServletResponse.SC_CREATED, "customer updated");
		return Response.status(200).entity(responseobj.toJson()).build();
	}
	
	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response loginCustomer(String request){
		ResponseObject responseobj=null;
		LoginRequest loginRequest=null;
		try{
			loginRequest = (LoginRequest)Unmarshaller.unmarshal(request, LoginRequest.class);
		}catch(UnmarshallingException ex){
			responseobj=new ResponseObject(loginRequest, javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST, "invalid request");
		}
		
		responseobj=new ResponseObject(loginRequest, 200, "test");
		return Response.status(200).entity(responseobj.toJson()).build();
	}

	@POST
	@Path("/test")
	@Consumes("application/json")
	@Produces("application/json")
	public Response testMethod()
			throws NamingException, ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchFieldException, SecurityException, IllegalArgumentException {
		
		entityManager = ResourceBase.INSTANCE.getEntityManager();
		
		ResponseObject responseobj = new ResponseObject(null, 200, "customer updated");
		return Response.status(200).entity(responseobj.toJson()).build();
	}

}
