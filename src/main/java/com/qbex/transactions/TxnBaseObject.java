package com.qbex.transactions;

import javax.persistence.EntityManager;

public class TxnBaseObject {
	protected EntityManager entityManager;

	public TxnBaseObject(EntityManager entityManager) {
		// TODO Auto-generated constructor stub
		this.setEntityManager(entityManager);
	}
	/**
	 * @return the entityManager
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * @param entityManager the entityManager to set
	 */
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
}
