package com.qbex.transactions;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityResult;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.SqlResultSetMapping;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.qbex.behaviour.BehaviourExtension;
import com.qbex.behaviour.TxnCustomerExtn;
import com.qbex.entity.QbexCustomer;
import com.qbex.pojo.Address;
import com.qbex.pojo.Customer;
import com.qbex.util.Helper;
import com.qbex.util.TxnConfigLoader;
import com.unmarshaller.Unmarshaller;

public class TxnCustomer extends TxnBaseObject {

	private BehaviourExtension<QbexCustomer> customerBehaviourExtn;

	public TxnCustomer(EntityManager entityManager)
			throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		super(entityManager);
		TxnConfigLoader loader = new TxnConfigLoader();
		InputStream resTxnConfig = this.getClass().getClassLoader().getResourceAsStream("META-INF/txnExtension.xml");
		try {
			loader.loadConfig(resTxnConfig);
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Class<?> temp = Class.forName("com.qbex.behaviour.TxnCustomerExtn");
		customerBehaviourExtn = (BehaviourExtension<QbexCustomer>) temp.newInstance();
		// System.out.println(customerBehaviourExtn.preIxn(null));
	}

	public QbexCustomer getCustomerById(String id) {
		QbexCustomer customer = null;
		entityManager.getTransaction().begin();
		try {
			Customer _customer = entityManager.find(Customer.class, id);
			customer=new QbexCustomer(_customer);
			Address address = new Address();
			address.setCustomerId(id);
			Query query = entityManager.createNativeQuery("select * from address where CUSTOMER_ID = '"+id+"'", Address.class);
			List<Address> addresses = query.getResultList();
			System.out.println("Addresses: "+addresses);
			System.out.println("Addresses: "+addresses.size());
			Set<Address> addressSet=new HashSet<Address>();
			for(Address a : addresses){
				addressSet.add(a);
				System.out.println(a.toJson());
			}
			customer.setAddresses(addressSet);
			entityManager.getTransaction().commit();
			System.out.println("Customer: " + customer);
		} catch (PersistenceException e) {
			System.out.println(e.getMessage());
			entityManager.getTransaction().rollback();
		}
		return customer;
	}

	public Customer insertCustomer(QbexCustomer customer) throws PersistenceException {
		// customerBehaviourExtn.preIxn(obj)
		if (customerBehaviourExtn != null) {
			customer = customerBehaviourExtn.preIxn(customer);
		}
		Timestamp currentTimestamp=Helper.getCurresntTimestamp();
		customer.setCreatedOn(currentTimestamp);
		customer.setUpdatedOn(currentTimestamp);
		entityManager.getTransaction().begin();
		try {
			String insertId = customer.insertCustomer(entityManager);
			customer.setId(insertId);
			entityManager.getTransaction().commit();
		} catch (PersistenceException e) {
			entityManager.getTransaction().rollback();
			throw e;
		}
		return customer;
	}
	
	public void updateCustomer(QbexCustomer customer) throws PersistenceException{
		entityManager.getTransaction().begin();
		try{
			customer.update(entityManager, customer.getId());
			entityManager.getTransaction().commit();
		}catch (PersistenceException e) {
			entityManager.getTransaction().rollback();
			throw e;
		}
	}
	
	public void matchPassword(String emailId, String password) throws PersistenceException{
		Customer customer=new Customer();
		entityManager.getTransaction().begin();
		try{
			entityManager.getTransaction().commit();
		}catch(PersistenceException e){
			entityManager.getTransaction().rollback();
			throw e;
		}
	}

	public void test(Customer customer) {
	}
}
