package com.qbex.util;

import java.util.ArrayList;
import java.util.List;

public class TransactionConfiguration {
	private List<Transaction> transactions;
	public TransactionConfiguration(){
		transactions=new ArrayList<Transaction>();
	}
	protected List<Transaction> getTransactions(){
		return this.transactions;
	}
	public Transaction getByName(String name){
		for (Transaction transaction : transactions) {
			if(name.equalsIgnoreCase(transaction.getName())){
				return transaction;
			}
		}
		return null;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TransactionConfiguration [transactions=" + transactions + "]";
	}
}
