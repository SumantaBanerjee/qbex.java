package com.qbex.util;

import java.util.ArrayList;
import java.util.List;

public class Action {
	private String name;
	private List<Behaviour> behaviours;
	public Action(){
		behaviours = new ArrayList<Behaviour>();
	}
	protected List<Behaviour> getBehaviours(){
		return this.behaviours;
	}
	public Behaviour getPreBehaviour(){
		for (Behaviour behaviour : behaviours) {
			if("pre".equalsIgnoreCase(behaviour.getMethod())){
				return behaviour;
			}
		}
		return null;
	}
	public Behaviour getPostBehaviour(){
		for (Behaviour behaviour : behaviours) {
			if("post".equalsIgnoreCase(behaviour.getMethod())){
				return behaviour;
			}
		}
		return null;
	}
	protected void setName(String _name){
		this.name=_name;
	}
	public String getName(){
		return this.name;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Action [name=" + name + ", behaviours=" + behaviours + "]";
	}
}
