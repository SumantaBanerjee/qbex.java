package com.qbex.util;

import java.util.ArrayList;
import java.util.List;

public class Transaction {
	private String name;
	private List<Action> actions;
	public Transaction(){
		this.actions=new ArrayList<Action>();
	}
	protected List<Action> getActions(){
		return this.actions;
	}
	public Action getActionByName(String name){
		for (Action action : actions) {
			if(name.equalsIgnoreCase(action.getName())){
				return action;
			}
		}
		return null;
	}
	protected void setName(String _name){
		this.name=_name;
	}
	public String getName(){
		return this.name;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Transaction [name=" + name + ", actions=" + actions + "]";
	}
}
