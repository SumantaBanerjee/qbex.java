package com.qbex.util;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class Helper {
	public static String getUniqueId(){
		return UUID.randomUUID().toString().replaceAll("-", "");
	}
	public static String generateSqlStatement(String table, Map<String, Object> map){
		String statement = "UPDATE "+table;
		Set<String> keySet = map.keySet();
		if(keySet.size()==0){
			return "";
		}
		statement+=" SET ";
		for (String key : keySet) {
			statement+=key+" = :"+key+", ";
		}
		return shapeQuery(statement);
	}
	public static String shapeQuery(String query){
		return query.replaceAll(", $", "");
	}
	public static Date getCurrentDate(){
		return  new java.sql.Date(Calendar.getInstance().getTime().getTime());
	}
	
	public static Timestamp getCurresntTimestamp(){
		return new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
	}
	public static String buildSelectQuery(List<String> params, HashMap<String, Object> where, String tableName) {
		String query="SELECT ";
		/*for (String param : params) {
			query+=tableName.charAt(0)+"."+param+", ";
		}*/
		//query=shapeQuery(query);
		query+="*";
		query+=" FROM "+tableName+" "+tableName.charAt(0)+" ";
		int i=0;
		if(where != null && where.size()>0){
			query+="WHERE ";
			Set<String> keySet=where.keySet();
			for(String key : keySet){
				i++;
				query+=key+" =:"+key;
				if(i!=where.size()){
					query+=" AND ";
				}
			}
		}
		return query;
	}
}
