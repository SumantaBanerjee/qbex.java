package com.qbex.util;

public class Behaviour {
	private String method;
	private String className;
	
	/**
	 * @return the method
	 */
	public String getMethod() {
		return method;
	}

	/**
	 * @param method the method to set
	 */
	protected void setMethod(String method) {
		this.method = method;
	}

	/**
	 * @return the className
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * @param className the className to set
	 */
	protected void setClassName(String className) {
		this.className = className;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Behaviour [method=" + method + ", className=" + className + "]";
	}
	
}
