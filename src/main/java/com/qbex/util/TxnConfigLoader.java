package com.qbex.util;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;



public class TxnConfigLoader {
	private static TransactionConfiguration txnConfiguration;
	public static void loadConfig(InputStream is) throws ParserConfigurationException, SAXException, IOException{
		txnConfiguration=new TransactionConfiguration();
		DocumentBuilderFactory builderFactory=DocumentBuilderFactory.newInstance();
		DocumentBuilder documentBuilder=builderFactory.newDocumentBuilder();
		Document document=documentBuilder.parse(is);
		document.getDocumentElement().normalize();
		NodeList transactions = document.getElementsByTagName("transaction");
		for(int i=0;i<transactions.getLength();i++){
			Element txnElement=(Element)transactions.item(i);
			Transaction transaction=new Transaction();
			String txnName = txnElement.getAttribute("name");
			transaction.setName(txnName);
			//Load Actions
			NodeList nodeActions=txnElement.getElementsByTagName("action");
			for(int j=0;j<nodeActions.getLength();j++){
				Element actionElement = (Element)nodeActions.item(j);
				Action action=new Action();
				String actionName=actionElement.getAttribute("name");
				action.setName(actionName);
				NodeList nodeBehaviours=actionElement.getElementsByTagName("behaviour");
				for(int k=0;k<nodeBehaviours.getLength();k++){
					Element behaviourElement=(Element)nodeBehaviours.item(k);
					String behaviourMethod=behaviourElement.getAttribute("method");
					String behaviourClass=behaviourElement.getTextContent().trim();
					Behaviour behaviour=new Behaviour();
					behaviour.setClassName(behaviourClass);
					behaviour.setMethod(behaviourMethod);
					action.getBehaviours().add(behaviour);
				}
				transaction.getActions().add(action);
			}
			txnConfiguration.getTransactions().add(transaction);
		}
		System.out.println(txnConfiguration);
	}
}
