package com.qbex.filters;
import javax.ws.rs.ext.Provider;

import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;

@Provider
public class AuthenticationFilter implements ContainerRequestFilter{

	public ContainerRequest filter(ContainerRequest request) {
		/*
		ResponseBuilder builder = null;
        String response = "Custom message";
        builder = Response.status(Response.Status.UNAUTHORIZED).entity(response);
        throw new WebApplicationException(builder.build());
        */
		return request;
	}
	
}
